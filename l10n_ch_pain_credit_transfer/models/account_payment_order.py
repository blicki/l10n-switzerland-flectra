# © 2013-2016 Akretion - Alexis de Lattre <alexis.delattre@akretion.com>
# © 2014 Serv. Tecnol. Avanzados - Pedro M. Baeza
# © 2016 Antiun Ingenieria S.L. - Antonio Espinosa
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

from flectra import models, api
from lxml import etree
import logging

logger = logging.getLogger(__name__)


class AccountPaymentOrder(models.Model):
    _inherit = 'account.payment.order'

    @api.model
    def generate_postal_address_block(self, party, party_type, partner, gen_args):
        logger.info('############################### generate_postal_address_block(party=%s, party_type=%s, '
                    'partner=%s, gen_args=%s) called' % (party, party_type, partner, gen_args))
        # Empfehlung Swiss Payment Standards: PstlAdr nicht verwenden bei Dbtr
        if party_type == 'Cdtr' and partner.country_id:
            postal_address = etree.SubElement(party, 'PstlAdr')
            if gen_args.get('pain_flavor').startswith(
                    'pain.001.001.') or gen_args.get('pain_flavor').startswith(
                    'pain.008.001.'):
                if partner.street:
                    streetName = etree.SubElement(postal_address, 'StrtNm')
                    streetName.text = self._prepare_field(
                        'Street Name', 'partner.street',
                        {'partner': partner}, 70, gen_args=gen_args)
                if partner.zip:
                    pstcd = etree.SubElement(postal_address, 'PstCd')
                    pstcd.text = self._prepare_field(
                        'Postal Code', 'partner.zip',
                        {'partner': partner}, 16, gen_args=gen_args)
                if partner.city:
                    twnnm = etree.SubElement(postal_address, 'TwnNm')
                    twnnm.text = self._prepare_field(
                        'Town Name', 'partner.city',
                        {'partner': partner}, 35, gen_args=gen_args)
            else:
                if partner.street:
                    adrline1 = etree.SubElement(postal_address, 'AdrLine')
                    adrline1.text = self._prepare_field(
                        'Adress Line1', 'partner.street',
                        {'partner': partner}, 70, gen_args=gen_args)
            country = etree.SubElement(postal_address, 'Ctry')
            country.text = self._prepare_field(
                'Country', 'partner.country_id.code',
                {'partner': partner}, 2, gen_args=gen_args)
