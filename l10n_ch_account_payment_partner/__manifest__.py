{
    "name": "Account Payment Partner for Switzerland",
    "version": "2.0.1.0.0",
    "category": "Banking addons",
    "license": "AGPL-3",
    "summary": "Set partner bank on outgoing invoices",
    "author": "Jamotion GmbH",
    "website": "https://jamotion.ch",
    "depends": ["account_payment_partner", "l10n_ch"],
    "data": [],
    "demo": [],
    "installable": True,
    "auto_install": True,
}
