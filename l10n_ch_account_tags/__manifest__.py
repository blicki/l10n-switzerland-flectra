# Copyright 2019 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    "name": "Switzerland Account Tags",
    "category": "Localisation",
    "summary": "",
    "version": "2.0.1.0.0",
    "author": "Camptocamp SA, Odoo Community Association (OCA), 2BIT GmbH",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["l10n_ch"],
    "data": [
        "data/new/account.account.tag.csv",
        "data/new/account.account.template.csv",
        "data/update/account.account.template.csv",
    ],
}
