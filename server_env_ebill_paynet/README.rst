===================================
Server environment for Ebill Paynet
===================================

.. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! This file is generated by oca-gen-addon-readme !!
   !! changes will be overwritten.                   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 

This module is based on the `server_environment` module to use files for
configuration. So we can have a different configuration for each
environment (dev, test, integration, prod).  This module define the config
variables for the `ebill_paynet` module.

Exemple of the section to put in the configuration file::

    [paynet_service.name_of_the_service]
    use_test_service": True,
    client_pid": 123456789,
    service_type": b2b,
    username": username,
    password": password,

**Table of contents**

.. contents::
   :local:

Installation
============

This module depends on:

- `server_environment` which is located on `OCA/server-env`
- `ebill_paynet`

So, please be sure that those modules have been installed properly on the system

Credits
=======

Authors
~~~~~~~

* Camptocamp

Contributors
        ------------

        * Jamotion <info@jamotion.ch>
* Thierry Ducrest <thierry.ducrest@camptocamp.com>
* `Trobz <https://trobz.com>`_:
  * Dung Tran <dungtd@trobz.com>
  * Hai Le Nguyen <hailn@trobz.com>

Other credits
~~~~~~~~~~~~~

The migration of this module from 13.0 to 14.0 as financially supported by Camptocamp