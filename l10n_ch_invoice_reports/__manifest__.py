# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Switzerland - Invoice Reports with payment option",
    "summary": "Extend invoice to add ISR/QR payment slip",
    "version": "2.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA), 2BIT GmbH",
    "category": "Localization",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["account", "l10n_ch", "web"],
    "data": ["data/reports.xml"],
    "auto_install": False,
    "installable": True,
}
