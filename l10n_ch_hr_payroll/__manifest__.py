
{
    'name': 'Switzerland - Payroll Flectra',
    'summary': 'Switzerland Payroll Rules for Flectra 2.0',
    'category': 'Localization',
    'author': "Open Net Sàrl,Odoo Community Association (OCA), Jamotion GmbH",
    'depends': [
        'hr_payroll',
        'hr_payroll_account',
        'hr_contract',
        'hr_attendance'
    ],
    'version': '2.0.0.0.0',
    'auto_install': False,
    'demo': [],
    'website': '',
    'license': 'AGPL-3',
    'data': [
        'security/ir.model.access.csv',
        'data/decimal.precision.xml',
        'data/hr.salary.rule.category.xml',
        'data/hr.salary.rule.xml',
        'views/hr_salary_rule_view.xml',
        'views/hr_employee_view.xml',
        'views/hr_contract_view.xml',
        'views/hr_payroll_view.xml',
        'views/hr_payroll_config_view.xml',
        'views/hr_payslip_view.xml',
        'views/hr_payslip_line_view.xml',
        'views/lpp_contract_view.xml'
    ],
    'installable': True
}
