from . import hr_payslip
from . import hr_salary_rule
from . import hr_salary_rule_category
from . import hr_employee
from . import res_company
from . import hr_payroll_config
from . import hr_contract
