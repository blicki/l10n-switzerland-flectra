# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import fields, models


class Company(models.Model):
    _inherit = "res.company"

    l10n_add_isr_pdf = fields.Boolean(
            string='ISR-PDF',
    )
    l10n_add_qr_pdf = fields.Boolean(
            string='QR-PDF',
    )
    l10n_ignore_qr_when_reconciled = fields.Boolean(
            string='Ignore QR when reconciled',
            help='Do not add QR Bill when invoice is reconciled',
    )
