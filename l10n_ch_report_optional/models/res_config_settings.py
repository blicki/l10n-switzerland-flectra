# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.

from flectra import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    l10n_add_isr_pdf = fields.Boolean(
            related='company_id.l10n_add_isr_pdf',
            readonly=False,
    )
    l10n_add_qr_pdf = fields.Boolean(
            related='company_id.l10n_add_qr_pdf',
            readonly=False,
    )
    l10n_ignore_qr_when_reconciled = fields.Boolean(
            related='company_id.l10n_ignore_qr_when_reconciled',
            readonly=False,
    )
