# Copyright 2012-2019 Camptocamp
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Switzerland - Bank type",
    "summary": "Types and number validation for swiss electronic pmnt. DTA, ESR",
    "version": "2.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA), 2BIT GmbH",
    "category": "Localization",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["base_iban", "l10n_ch"],
    "data": ["views/bank.xml"],
    "auto_install": False,
    "installable": True,
}
